<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecepieTranslation
 *
 * @ORM\Table(name="recepie_translation", indexes={@ORM\Index(name="fk_post_translations_post1_idx", columns={"recepie_id"}), @ORM\Index(name="fk_post_translation_language1_idx", columns={"language_id"})})
 * @ORM\Entity
 */
class RecepieTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=45, nullable=true)
     */
    private $title;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var \Recepie
     *
     * @ORM\ManyToOne(targetEntity="Recepie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recepie_id", referencedColumnName="id")
     * })
     */
    private $recepie;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return RecepieTranslation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return RecepieTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set recepie
     *
     * @param \AppBundle\Entity\Recepie $recepie
     * @return RecepieTranslation
     */
    public function setRecepie(\AppBundle\Entity\Recepie $recepie = null)
    {
        $this->recepie = $recepie;

        return $this;
    }

    /**
     * Get recepie
     *
     * @return \AppBundle\Entity\Recepie 
     */
    public function getRecepie()
    {
        return $this->recepie;
    }
}
