<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IngredientCategoryTranslation
 *
 * @ORM\Table(name="ingredient_category_translation", indexes={@ORM\Index(name="fk_ingredient_translations_ingredient_category1_idx", columns={"ingredient_category_id"}), @ORM\Index(name="fk_ingredient_category_translation_language1_idx", columns={"language_id"})})
 * @ORM\Entity
 */
class IngredientCategoryTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=45, nullable=false)
     */
    private $name;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var \IngredientCategory
     *
     * @ORM\ManyToOne(targetEntity="IngredientCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ingredient_category_id", referencedColumnName="id")
     * })
     */
    private $ingredientCategory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return IngredientCategoryTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return IngredientCategoryTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set ingredientCategory
     *
     * @param \AppBundle\Entity\IngredientCategory $ingredientCategory
     * @return IngredientCategoryTranslation
     */
    public function setIngredientCategory(\AppBundle\Entity\IngredientCategory $ingredientCategory = null)
    {
        $this->ingredientCategory = $ingredientCategory;

        return $this;
    }

    /**
     * Get ingredientCategory
     *
     * @return \AppBundle\Entity\IngredientCategory 
     */
    public function getIngredientCategory()
    {
        return $this->ingredientCategory;
    }
}
