<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ingredient
 *
 * @ORM\Table(name="ingredient", indexes={@ORM\Index(name="fk_ingredient_ingredient_category1_idx", columns={"ingredient_category_id"}), @ORM\Index(name="fk_ingredient_ingredient_multiplycity1_idx", columns={"ingredient_multiplycity_id"})})
 * @ORM\Entity
 */
class Ingredient
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="calories", type="integer", nullable=true)
     */
    private $calories;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredientcol", type="string", length=45, nullable=true)
     */
    private $ingredientcol;

    /**
     * @var string
     *
     * @ORM\Column(name="ingredientcol1", type="string", length=45, nullable=true)
     */
    private $ingredientcol1;

    /**
     * @var \IngredientCategory
     *
     * @ORM\ManyToOne(targetEntity="IngredientCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ingredient_category_id", referencedColumnName="id")
     * })
     */
    private $ingredientCategory;

    /**
     * @var \IngredientMultiplycity
     *
     * @ORM\ManyToOne(targetEntity="IngredientMultiplycity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ingredient_multiplycity_id", referencedColumnName="id")
     * })
     */
    private $ingredientMultiplycity;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Recepie", mappedBy="ingredient")
     */
    private $post;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->post = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set calories
     *
     * @param integer $calories
     * @return Ingredient
     */
    public function setCalories($calories)
    {
        $this->calories = $calories;

        return $this;
    }

    /**
     * Get calories
     *
     * @return integer 
     */
    public function getCalories()
    {
        return $this->calories;
    }

    /**
     * Set ingredientcol
     *
     * @param string $ingredientcol
     * @return Ingredient
     */
    public function setIngredientcol($ingredientcol)
    {
        $this->ingredientcol = $ingredientcol;

        return $this;
    }

    /**
     * Get ingredientcol
     *
     * @return string 
     */
    public function getIngredientcol()
    {
        return $this->ingredientcol;
    }

    /**
     * Set ingredientcol1
     *
     * @param string $ingredientcol1
     * @return Ingredient
     */
    public function setIngredientcol1($ingredientcol1)
    {
        $this->ingredientcol1 = $ingredientcol1;

        return $this;
    }

    /**
     * Get ingredientcol1
     *
     * @return string 
     */
    public function getIngredientcol1()
    {
        return $this->ingredientcol1;
    }

    /**
     * Set ingredientCategory
     *
     * @param \AppBundle\Entity\IngredientCategory $ingredientCategory
     * @return Ingredient
     */
    public function setIngredientCategory(\AppBundle\Entity\IngredientCategory $ingredientCategory = null)
    {
        $this->ingredientCategory = $ingredientCategory;

        return $this;
    }

    /**
     * Get ingredientCategory
     *
     * @return \AppBundle\Entity\IngredientCategory 
     */
    public function getIngredientCategory()
    {
        return $this->ingredientCategory;
    }

    /**
     * Set ingredientMultiplycity
     *
     * @param \AppBundle\Entity\IngredientMultiplycity $ingredientMultiplycity
     * @return Ingredient
     */
    public function setIngredientMultiplycity(\AppBundle\Entity\IngredientMultiplycity $ingredientMultiplycity = null)
    {
        $this->ingredientMultiplycity = $ingredientMultiplycity;

        return $this;
    }

    /**
     * Get ingredientMultiplycity
     *
     * @return \AppBundle\Entity\IngredientMultiplycity 
     */
    public function getIngredientMultiplycity()
    {
        return $this->ingredientMultiplycity;
    }

    /**
     * Add post
     *
     * @param \AppBundle\Entity\Recepie $post
     * @return Ingredient
     */
    public function addPost(\AppBundle\Entity\Recepie $post)
    {
        $this->post[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \AppBundle\Entity\Recepie $post
     */
    public function removePost(\AppBundle\Entity\Recepie $post)
    {
        $this->post->removeElement($post);
    }

    /**
     * Get post
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPost()
    {
        return $this->post;
    }
}
