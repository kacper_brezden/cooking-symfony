<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IngredientMultiplycityTranslation
 *
 * @ORM\Table(name="ingredient_multiplycity_translation", indexes={@ORM\Index(name="fk_ingredient_multiplycity_translation_ingredient_multiplyc_idx", columns={"ingredient_multiplycity_id"}), @ORM\Index(name="fk_ingredient_multiplycity_translation_language1_idx", columns={"language_id"})})
 * @ORM\Entity
 */
class IngredientMultiplycityTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \IngredientMultiplycity
     *
     * @ORM\ManyToOne(targetEntity="IngredientMultiplycity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ingredient_multiplycity_id", referencedColumnName="id")
     * })
     */
    private $ingredientMultiplycity;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return IngredientMultiplycityTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ingredientMultiplycity
     *
     * @param \AppBundle\Entity\IngredientMultiplycity $ingredientMultiplycity
     * @return IngredientMultiplycityTranslation
     */
    public function setIngredientMultiplycity(\AppBundle\Entity\IngredientMultiplycity $ingredientMultiplycity = null)
    {
        $this->ingredientMultiplycity = $ingredientMultiplycity;

        return $this;
    }

    /**
     * Get ingredientMultiplycity
     *
     * @return \AppBundle\Entity\IngredientMultiplycity 
     */
    public function getIngredientMultiplycity()
    {
        return $this->ingredientMultiplycity;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return IngredientMultiplycityTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
