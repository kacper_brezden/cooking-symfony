<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BlogCategoryTranslation
 *
 * @ORM\Table(name="blog_category_translation", indexes={@ORM\Index(name="fk_blog_category_translations_blog_category1_idx", columns={"blog_category_id"}), @ORM\Index(name="fk_blog_category_translation_language1_idx", columns={"language_id"})})
 * @ORM\Entity
 */
class BlogCategoryTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="default", type="boolean", nullable=false)
     */
    private $default;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var \BlogCategory
     *
     * @ORM\ManyToOne(targetEntity="BlogCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="blog_category_id", referencedColumnName="id")
     * })
     */
    private $blogCategory;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set default
     *
     * @param boolean $default
     * @return BlogCategoryTranslation
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return boolean 
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return BlogCategoryTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return BlogCategoryTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set blogCategory
     *
     * @param \AppBundle\Entity\BlogCategory $blogCategory
     * @return BlogCategoryTranslation
     */
    public function setBlogCategory(\AppBundle\Entity\BlogCategory $blogCategory = null)
    {
        $this->blogCategory = $blogCategory;

        return $this;
    }

    /**
     * Get blogCategory
     *
     * @return \AppBundle\Entity\BlogCategory 
     */
    public function getBlogCategory()
    {
        return $this->blogCategory;
    }
}
