<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Blog
 *
 * @ORM\Table(name="blog", indexes={@ORM\Index(name="fk_blog_user_idx", columns={"user_id"}), @ORM\Index(name="fk_blog_blog_category1_idx", columns={"blog_category_id"})})
 * @ORM\Entity
 */
class Blog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \BlogCategory
     *
     * @ORM\ManyToOne(targetEntity="BlogCategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="blog_category_id", referencedColumnName="id")
     * })
     */
    private $blogCategory;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param integer $createdAt
     * @return Blog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return integer 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param integer $updatedAt
     * @return Blog
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return integer 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set blogCategory
     *
     * @param \AppBundle\Entity\BlogCategory $blogCategory
     * @return Blog
     */
    public function setBlogCategory(\AppBundle\Entity\BlogCategory $blogCategory = null)
    {
        $this->blogCategory = $blogCategory;

        return $this;
    }

    /**
     * Get blogCategory
     *
     * @return \AppBundle\Entity\BlogCategory 
     */
    public function getBlogCategory()
    {
        return $this->blogCategory;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\User $user
     * @return Blog
     */
    public function setUser(\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
