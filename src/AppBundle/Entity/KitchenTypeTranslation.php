<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * KitchenTypeTranslation
 *
 * @ORM\Table(name="kitchen_type_translation", indexes={@ORM\Index(name="fk_kitchen_type_translation_kitchen_type1_idx", columns={"kitchen_type_id"}), @ORM\Index(name="fk_kitchen_type_translation_language1_idx", columns={"language_id"})})
 * @ORM\Entity
 */
class KitchenTypeTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var \KitchenType
     *
     * @ORM\ManyToOne(targetEntity="KitchenType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="kitchen_type_id", referencedColumnName="id")
     * })
     */
    private $kitchenType;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return KitchenTypeTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set kitchenType
     *
     * @param \AppBundle\Entity\KitchenType $kitchenType
     * @return KitchenTypeTranslation
     */
    public function setKitchenType(\AppBundle\Entity\KitchenType $kitchenType = null)
    {
        $this->kitchenType = $kitchenType;

        return $this;
    }

    /**
     * Get kitchenType
     *
     * @return \AppBundle\Entity\KitchenType 
     */
    public function getKitchenType()
    {
        return $this->kitchenType;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return KitchenTypeTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
