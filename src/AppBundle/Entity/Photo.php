<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Photo
 *
 * @ORM\Table(name="photo")
 * @ORM\Entity
 */
class Photo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=255, nullable=false)
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail", type="string", length=255, nullable=false)
     */
    private $thumbnail;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer", nullable=false)
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer", nullable=false)
     */
    private $updatedAt;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Recepie", mappedBy="photo")
     */
    private $recepie;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="RecepieStep", mappedBy="photo")
     */
    private $recepieStep;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recepie = new \Doctrine\Common\Collections\ArrayCollection();
        $this->recepieStep = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     *
     * @param string $file
     * @return Photo
     */
    public function setFile($file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return string 
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set thumbnail
     *
     * @param string $thumbnail
     * @return Photo
     */
    public function setThumbnail($thumbnail)
    {
        $this->thumbnail = $thumbnail;

        return $this;
    }

    /**
     * Get thumbnail
     *
     * @return string 
     */
    public function getThumbnail()
    {
        return $this->thumbnail;
    }

    /**
     * Set createdAt
     *
     * @param integer $createdAt
     * @return Photo
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return integer 
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param integer $updatedAt
     * @return Photo
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return integer 
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add recepie
     *
     * @param \AppBundle\Entity\Recepie $recepie
     * @return Photo
     */
    public function addRecepie(\AppBundle\Entity\Recepie $recepie)
    {
        $this->recepie[] = $recepie;

        return $this;
    }

    /**
     * Remove recepie
     *
     * @param \AppBundle\Entity\Recepie $recepie
     */
    public function removeRecepie(\AppBundle\Entity\Recepie $recepie)
    {
        $this->recepie->removeElement($recepie);
    }

    /**
     * Get recepie
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecepie()
    {
        return $this->recepie;
    }

    /**
     * Add recepieStep
     *
     * @param \AppBundle\Entity\RecepieStep $recepieStep
     * @return Photo
     */
    public function addRecepieStep(\AppBundle\Entity\RecepieStep $recepieStep)
    {
        $this->recepieStep[] = $recepieStep;

        return $this;
    }

    /**
     * Remove recepieStep
     *
     * @param \AppBundle\Entity\RecepieStep $recepieStep
     */
    public function removeRecepieStep(\AppBundle\Entity\RecepieStep $recepieStep)
    {
        $this->recepieStep->removeElement($recepieStep);
    }

    /**
     * Get recepieStep
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRecepieStep()
    {
        return $this->recepieStep;
    }
}
