<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecepieStep
 *
 * @ORM\Table(name="recepie_step", indexes={@ORM\Index(name="fk_recepie_step_post1_idx", columns={"recepie_id"})})
 * @ORM\Entity
 */
class RecepieStep
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Recepie
     *
     * @ORM\ManyToOne(targetEntity="Recepie")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recepie_id", referencedColumnName="id")
     * })
     */
    private $recepie;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="Photo", inversedBy="recepieStep")
     * @ORM\JoinTable(name="recepie_step_has_photo",
     *   joinColumns={
     *     @ORM\JoinColumn(name="recepie_step_id", referencedColumnName="id")
     *   },
     *   inverseJoinColumns={
     *     @ORM\JoinColumn(name="photo_id", referencedColumnName="id")
     *   }
     * )
     */
    private $photo;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->photo = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set recepie
     *
     * @param \AppBundle\Entity\Recepie $recepie
     * @return RecepieStep
     */
    public function setRecepie(\AppBundle\Entity\Recepie $recepie = null)
    {
        $this->recepie = $recepie;

        return $this;
    }

    /**
     * Get recepie
     *
     * @return \AppBundle\Entity\Recepie 
     */
    public function getRecepie()
    {
        return $this->recepie;
    }

    /**
     * Add photo
     *
     * @param \AppBundle\Entity\Photo $photo
     * @return RecepieStep
     */
    public function addPhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photo[] = $photo;

        return $this;
    }

    /**
     * Remove photo
     *
     * @param \AppBundle\Entity\Photo $photo
     */
    public function removePhoto(\AppBundle\Entity\Photo $photo)
    {
        $this->photo->removeElement($photo);
    }

    /**
     * Get photo
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
