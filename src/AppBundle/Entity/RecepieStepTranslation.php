<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RecepieStepTranslation
 *
 * @ORM\Table(name="recepie_step_translation", indexes={@ORM\Index(name="fk_recepie_step_translation_recepie_step1_idx", columns={"recepie_step_id"}), @ORM\Index(name="fk_recepie_step_translation_language1_idx", columns={"language_id"})})
 * @ORM\Entity
 */
class RecepieStepTranslation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var \Language
     *
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var \RecepieStep
     *
     * @ORM\ManyToOne(targetEntity="RecepieStep")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recepie_step_id", referencedColumnName="id")
     * })
     */
    private $recepieStep;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RecepieStepTranslation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return RecepieStepTranslation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set language
     *
     * @param \AppBundle\Entity\Language $language
     * @return RecepieStepTranslation
     */
    public function setLanguage(\AppBundle\Entity\Language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AppBundle\Entity\Language 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set recepieStep
     *
     * @param \AppBundle\Entity\RecepieStep $recepieStep
     * @return RecepieStepTranslation
     */
    public function setRecepieStep(\AppBundle\Entity\RecepieStep $recepieStep = null)
    {
        $this->recepieStep = $recepieStep;

        return $this;
    }

    /**
     * Get recepieStep
     *
     * @return \AppBundle\Entity\RecepieStep 
     */
    public function getRecepieStep()
    {
        return $this->recepieStep;
    }
}
