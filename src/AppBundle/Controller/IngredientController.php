<?php

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\FOSRestController;

/**
 * Description of IngredientController
 *
 * @author rebzden
 */

class IngredientController extends FOSRestController
{

    public function getAllAction()
    {
        $data = [
            'type'     => 'Spicy',
            'quantity' => '30ml',
        ];
        $view = $this->view($data, 200);
        return $this->handleView($view);
    }

}
